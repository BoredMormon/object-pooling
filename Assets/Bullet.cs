﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	// Use this for initialization
	void OnEnable () {
        Invoke("DisableMe", 3);
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += Vector3.up * Time.deltaTime;
	}

    void DisableMe()
    {
        gameObject.SetActive(false);
    }
}
